<?php
add_action( 'init', 'register_post_types' );
function register_post_types(){
	register_post_type('portfolio', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Портфолио', // основное название для типа записи
			'singular_name'      => 'Кейс', // название для одной записи этого типа
			'add_new'            => 'Добавить кейс', // для добавления новой записи
			'add_new_item'       => 'Добавление кейса', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование кейса', // для редактирования типа записи
			'new_item'           => 'Новый  кейс', // текст новой записи
			'view_item'          => 'Смотреть кейс', // для просмотра записи этого типа.
			'search_items'       => 'Искать кейс', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Портфолио', // название меню
		),
		'public'              => true,
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'menu_icon'           => 'dashicons-archive',
		'hierarchical'        => false,
		'supports'            => array('title', 'thumbnail'),
		'has_archive'         => true,
		'rewrite'             => array('slug' => 'portfolio'),
		'query_var'           => true,
	) );

	register_post_type('c_forms', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Формы', // основное название для типа записи
			'singular_name'      => 'Форма', // название для одной записи этого типа
			'add_new'            => 'Добавить форму', // для добавления новой записи
			'add_new_item'       => 'Добавление форму', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование формы', // для редактирования типа записи
			'new_item'           => 'Новая форма', // текст новой записи
			'view_item'          => 'Смотреть форму', // для просмотра записи этого типа.
			'search_items'       => 'Искать форму', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Формы', // название меню
		),
		'public'              => true,
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'menu_icon'           => 'dashicons-list-view',
		'hierarchical'        => false,
		'supports'            => array('title'),
		'has_archive'         => false,
		'query_var'           => true,
	) );

	register_post_type('reviews', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Отзывы',
			'singular_name'      => 'Отзыв',
			'add_new'            => 'Добавить отзыв',
			'add_new_item'       => 'Добавление отзыва',
			'edit_item'          => 'Редактирование отзыва',
			'new_item'           => 'Новый  отзыв',
			'view_item'          => 'Смотреть отзыв',
			'search_items'       => 'Искать отзыв',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Отзывы',
		),
		'public'              => true,
		'show_in_menu'        => true,
		'menu_icon'           => 'dashicons-admin-comments',
		'supports'            => array('title', 'thumbnail', 'editor'),
		'has_archive'         => true,
		'query_var'           => true,
	) );

	register_post_type('products', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Товары',
			'singular_name'      => 'Товар',
			'add_new'            => 'Добавить товар',
			'add_new_item'       => 'Добавление товара',
			'edit_item'          => 'Редактирование товара',
			'new_item'           => 'Новый  товар',
			'view_item'          => 'Смотреть товар',
			'search_items'       => 'Искать товар',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Товары',
		),
		'public'              => true,
		'show_in_menu'        => true,
		'menu_icon'           => 'dashicons-screenoptions',
		'hierarchical'        => false,
		'supports'            => array('title', 'thumbnail'),
		'has_archive'         => true,
		'rewrite'             => array('slug' => 'products'),
		'query_var'           => true,
	) );
}

add_action( 'init', 'create_taxonomies' );

// функция, создающая 2 новые таксономии "genres" и "writers" для постов типа "book"
function create_taxonomies(){

	// Добавляем древовидную таксономию 'genre' (как категории)
	register_taxonomy('categories', array('products'), array(
		'hierarchical'  => true,
		'labels'        => array(
			'name'              => 'Категории',
			'singular_name'     => 'Категория',
			'search_items'      => 'Искать категорию',
			'all_items'         => 'Все категории',
			'parent_item'       => 'Категория',
			'parent_item_colon' => 'Категория',
			'edit_item'         => 'Редактировать категорию',
			'update_item'       => 'Обновить категория',
			'add_new_item'      => 'Новая категория',
			'new_item_name'     => 'Новая категория',
			'menu_name'         => 'Категории',
		),
		'show_ui'       => true,
		'query_var'     => true
	));

	register_taxonomy('reviews_category', array('reviews'), array(
		'hierarchical'  => true,
		'labels'        => array(
			'name'              => 'Тип',
			'singular_name'     => 'Тип',
			'search_items'      => 'Искать тип',
			'all_items'         => 'Все типы',
			'parent_item'       => 'Тип',
			'parent_item_colon' => 'тип',
			'edit_item'         => 'Редактировать тип',
			'update_item'       => 'Обновить тип',
			'add_new_item'      => 'Новый тип',
			'new_item_name'     => 'Новый тип',
			'menu_name'         => 'Тип',
		),
		'show_ui'       => true,
		'query_var'     => true
	));
}
