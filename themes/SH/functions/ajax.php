<?php
add_action('wp_ajax_review', 'new_review');
add_action('wp_ajax_nopriv_review', 'new_review');

function new_review () {
  $post_data = array(
  	'post_title'    => 'Новый отзыв',
  	'post_content'  => $_POST['comment'] || ' ',
  	'post_status'   => 'pending',
  	'post_author'   => 1,
    'post_type'     => 'reviews',
    'meta_input'    => array(
      'author_name'   => $_POST['name'],
      'review_town'   => $_POST['town']
    ),
  );

  // Вставляем запись в базу данных
  $post_id = wp_insert_post( $post_data );
  $gallery = [];
  foreach ($_FILES as $key => $value) {
    $gallery[] = media_handle_upload($key, $post_id);
  }
  update_field( 'review_images', $gallery , $post_id );
  wp_die();
}


add_action('wp_ajax_get_fitting', 'get_fitting');
add_action('wp_ajax_nopriv_get_fitting', 'get_fitting');

function get_fitting () {
  $result = get_post($_POST['id']);
  $result->big_image = get_field('head_img', $_POST['id']);
  $result->discription = get_field('discription', $_POST['id']);
  $result->subname = get_field('subtitle', $_POST['id']);
  echo json_encode($result);
  wp_die();

}
