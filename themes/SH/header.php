<!DOCTYPE html>
<html>
  <head>
        <title><?php wp_title('',true);?></title>
        <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" type="image/x-icon">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
      <div class="container">
        <header class="header">
          <div class="fl-row">
            <div class="fl-coll header__logo wow fadeInLeft"><?=get_custom_logo()?></div>
            <div class="fl-coll header__nav wow fadeIn">
              <nav>
                <button class="burger">
                  <span></span>
                </button>
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'primary',
                  	'menu_class'      => 'nav__list',
                  	'menu_id'         => '',
                  	'echo'            => true,
                  	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                  ) );
                ?>
              </nav>
            </div>
            <?php
              $ptn = "/[^0-9]/";
              $telOrigin = get_field('header_tel', 'option');
              $rpltxt = "";
              $headerTel = preg_replace($ptn, $rpltxt, $telOrigin);
            ?>
            <div class="fl-coll header__right wow fadeInRight"><a href="tel:<?=$headerTel?>"><?=$telOrigin?></a>
              <button class="btn modal-open" data-modal="#backCall">Вызвать замерщика</button>
            </div>
          </div>
        </header>
      </div>
