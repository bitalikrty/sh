<div class="footer-container">
  <div class="container">
    <footer class="footer">
      <div class="footer__row">
        <div class="footer__col">
          <?=get_custom_logo()?>
          <div class="coppiring"><?=the_field('coppiring', 'option')?></div>
          <p><?=the_field('footer-left-text', 'option')?></p>
        </div>
        <div class="footer__col">
          <div class="row">
            <?php
              if( have_rows('footer_menu', 'option') ):
                while ( have_rows('footer_menu', 'option') ) : the_row();?>
                <div class="col-4">
                  <?php while ( have_rows('li1', 'option') ) : the_row();?>
                      <ul class="footer__ul">
                        <li>
                          <a href="<?=the_sub_field('сылка_заголовка')?>"><?=the_sub_field('title')?></a>
                          <ul>
                            <?php while ( have_rows('li2', 'option') ) : the_row();?>
                              <li> <a href="<?=the_sub_field('url')?>"><?=the_sub_field('title')?></a> </li>
                            <?php endwhile;?>
                          </ul>
                        </li>
                      </ul>
                  <?php endwhile;?>
                </div>
            <?php endwhile;endif;?>
          </div>
        </div>
        <?php
          $ptn = "/[^0-9]/";
          $telOrigin = get_field('header_tel', 'option');
          $rpltxt = "";
          $headerTel = preg_replace($ptn, $rpltxt, $telOrigin);
        ?>
        <div class="footer__col"> <a class="footer__tel" href="tel:<?=$headerTel?>"><?=$telOrigin?></a>
          <div class="footer__soc">
            <p>Подписывайтесь на наши социальные сети:</p>
            <?php while ( have_rows('footer_soc', 'option') ) : the_row();?>
              <a class="soc__item" href="<?=the_sub_field('url')?>"><img src="<?=the_sub_field('icon')?>"/></a>
            <?php endwhile;?>
          </div>
          <a class="link" href="#">Политика <br> конфиденциальности</a>
          <p>Сайт разработан в студии <a class="link" href="#">«Mandzhi Group»</a></p>
        </div>
      </div>
    </footer>
  </div>
</div>
<div class="overflow"></div>

<?php get_template_part('paths/modals')?>
<script>
window.ajaxUrl = '<?php echo admin_url("admin-ajax.php") ?>'
</script>
<?php wp_footer(); ?>
</body>
</html>
