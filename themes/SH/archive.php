<?php get_header();?>
<div class="container news-page">
  <?php page_head(array(
    'title'     => get_field('news_title', 'option'),
    'subtitle'  => get_field('news_subtitle', 'option'),
    'img'       => get_field('news_image', 'option'),
    'small'     => false
  ))?>

  <?php text_block(array(
    'title'    => false,
    'border'   => false,
    'subtitle' => false,
    'text'     => get_field('news_text_block', 'option')
  ))?>

  <?php
    $cases = get_posts(array());
  ?>
  <div class="entrence-images mt-50 mb-100">
    <div class="title-block">
      <div class="h1">В КАДРАХ</div>
      <p class="mb-50">интересные моменты из нашей жизни</p>
    </div>
    <div class="row">
      <?php foreach( $cases as $post ): setup_postdata($post);?>
      	<div class="col-6">
          <div class="block">
            <div class="block__img" style="background-image: url(<?=get_the_post_thumbnail_url(get_the_ID(), 'full')?>)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title"><b><?=the_title()?></b></div>
              </div>
              <div class="right"> <a href="<?php the_permalink()?>" class="btn">Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
    	<?php endforeach; wp_reset_postdata();?>
    </div>
  </div>
</div>
<?php get_footer();?>
