<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">


  <div class="home container">
    <div class="title-block">
      <div class="h1"><?=the_field('title');?></div>
      <p><?=the_field('subtitle');?></p>
    </div>
    <div class="wrapper">
      <div class="home__slider slider slider-light">
        <?php
        $gallery = get_field('slider');
        foreach ($gallery as $key => $value):?>
          <div class="slider__item" style="background-image: url(<?=$value['url'];?>)"></div>
        <?php endforeach;?>
      </div>
      <div class="slider-text">
        <div class="left-block">
          <p>Площадь стены<b><?=the_field('area');?> кв.м.</b></p>
        </div>
        <div class="right-block">
          <div class="row">
            <p>Ожидаемый срок исполнения <b><?=the_field('excepted_day');?></b></p>
          </div>
          <div class="row">
            <p>Фактический срок исполнения <b><?=the_field('fact_day');?></b></p>
          </div>
        </div>
      </div>
    </div>

    <?php text_block(array(
      'title'    => get_field('text_block_title'),
      'border'   => get_field('text_block_border'),
      'subtitle' => get_field('text_block_subtitle'),
      'text'     => get_field('text_block_txt')
    ))?>

    <div class="case-review mb-100">
      <div class="title-block">
        <div class="h1">Отзыв</div>
        <p>заказчика о нашей работе</p>
      </div>
      <div class="row mt-50">
        <div class="col-5">
          <a href="<?=the_field('foto');?>" data-fancybox="data-fancybox">
            <img src="<?=the_field('foto');?>"/>
          </a>
        </div>
        <div class="col-7">
          <div class="small-title">
            <div class="title"><?=the_field('author_name');?></div>
            <p><?=the_field('text_after_name');?></p>
          </div>
          <?=the_field('review_text');?>
        </div>
      </div>
    </div>

    <?php
      $cases = get_posts(array(
        'post_type'   => 'portfolio',
        'numberposts' => 2
      ));
    ?>

    <div class="other-items">
      <div class="title-block">
        <div class="h1">Другие проекты</div>
        <p>проекты, которыми мы гордимся</p>
      </div>
      <div class="entrence-images mt-50 mb-100">
        <div class="row">
          <?php foreach( $cases as $post ): setup_postdata($post);?>
          	<div class="col-6">
              <div class="block">
                <div class="block__img" style="background-image: url(<?=get_the_post_thumbnail_url(get_the_ID(), 'full')?>)"></div>
                <div class="block__content">
                  <div class="left">
                    <div class="title"><b><?=the_title()?></b></div>
                  </div>
                  <div class="right"> <a href="<?php the_permalink()?>" class="btn">Подробнее<i class="icon icon-right"></i></a></div>
                </div>
              </div>
            </div>
        	<?php endforeach; wp_reset_postdata();?>
        </div>
      </div>
    </div>
  </div>

</div>
<?php get_footer();?>
