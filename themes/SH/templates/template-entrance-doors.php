<?php /* Template Name: Page::Entrannce Door */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="entrence-plus mb-100 mt-50">
    <div class="row">
      <?php
      if( have_rows('plus') ):
      while ( have_rows('plus') ) : the_row();?>

        <div class="col-4">
          <div class="img-in-block"><img src="<?=the_sub_field('icon')?>"/></div>
          <div class="title"><?=the_sub_field('txt')?></div>
        </div>

      <?php endwhile; endif;?>
    </div>
  </div>
  <div class="entrence-images mb-100">
    <div class="row">
      <?php
      if( have_rows('fottos') ):
      while ( have_rows('fottos') ) : the_row();?>

        <a href="<?=the_sub_field('foto')?>" data-fancybox class="col-6">
          <div class="entrence-image" style="background-image: url(<?=the_sub_field('foto')?>)"></div>
        </a>

      <?php endwhile; endif;?>
    </div>
  </div>

  <div class="home__bform">
    <?php
      $form_id = get_field('form_id');
      $form_name = get_post_meta($form_id)['forms_0_id'][0];
      Forms::getFormById($form_name);
    ?>
  </div>

</div>
<?php get_footer();?>
