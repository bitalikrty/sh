<?php /* Template Name: Page::Buyers */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="entrence-images mt-50 mb-100">
    <div class="row">
      <?php
        if( have_rows('buyers_items') ):
        while ( have_rows('buyers_items') ) : the_row();
      ?>

      <div class="col-6">
        <div class="block">
          <div class="block__img" style="background-image: url(<?=the_sub_field('img')?>)"></div>
          <div class="block__content" style="min-height: 78px;">
            <div class="left">
              <div class="title"> <b><?=the_sub_field('text')?></b></div>
            </div>
            <div class="right"> <a href="<?=the_sub_field('url')?>" class="btn">
                 Подробнее<i class="icon icon-right"></i></a></div>
          </div>
        </div>
      </div>

      <?php endwhile;endif;?>
    </div>
  </div>

</div>
<?php get_footer();?>
