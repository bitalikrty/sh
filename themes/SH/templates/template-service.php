<?php /* Template Name: Page::Service */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="principles mb-50">
    <?php blocks_w_text(array(
      'blocks' => get_field('blocks_w_texts'),
      'ad_class' => 'lp',
    ))?>
  </div>

</div>

<div class="price-list mb-100 mt-100">
  <div class="container"> <img class="price__img" src="<?=the_field('price_image', 'option')?>"/>
    <div class="price__content">
      <div class="title">Скачайте прайс на услуги,</div>
      <p>чтобы узнать их приблизительную стоимость</p>
      <a href="<?=the_field('price_file', 'option')?>" download class="btn-green">Скачать прайс</a>
    </div>
  </div>
</div>

<?php get_footer();?>
