<?php /* Template Name: Page::Dillers */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <div class="dillers mt-50">
    <div class="dropdow dillers__dropdown">
      <div class="value">Выберите город</div>
      <ul class="list">
        <?php
        $i = 0;
        if( have_rows('dillers_town') ):
        while ( have_rows('dillers_town') ) : the_row();?>

          <li data-town="#town<?=$i; $i++?>"><?=the_sub_field('town')?></li>

        <?php endwhile; endif;?>
      </ul>
    </div>
    <div class="dillers__maps">
      <?php
      $i = 0;
      if( have_rows('dillers_town') ):
      while ( have_rows('dillers_town') ) : the_row();?>

        <div class="dillers__map <?php if(!$i) echo 'active'?>" id="town<?=$i?>">
          <div class="ya-map" id="map<?=$i; $i++?>" data-x="<?=the_sub_field('map_x')?>" data-y="<?=the_sub_field('map_y')?>"></div>
          <div class="info">
            <div class="title"><?the_sub_field('diller')?></div>
            <p> <b>Адрес:<br></b><?=the_sub_field('town')?>, <?=the_sub_field('street')?></p>
            <p><?=the_sub_field('contacts')?></p>
          </div>
        </div>

      <?php endwhile; endif;?>

    </div>
  </div>

  <div class="home__bform">
    <?php
      $form_id = get_field('form_id');
      $form_name = get_post_meta($form_id)['forms_0_id'][0];
      Forms::getFormById($form_name);
    ?>
  </div>

</div>


<?php get_footer();?>
