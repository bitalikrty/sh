jQuery(document).ready(function($) {
  var reviewsGallery = []

  var reviewForm = $(".send-reviews").validate({
  rules: {
    name: 'required',
    comment: 'required'
  },
  messages: {
    name: 'Заполните поле',
    comment: 'Заполните поле'
  },
  invalidHandler: function () {
    return false
  },
  submitHandler: function(form) {
    // some other code
    // maybe disabling submit button
    // then:
    var data = new FormData();
    data.append('name', $(form).find('[name="name"]').val())
    data.append('town', $(form).find('[name="town"]').val())
    data.append('action', $(form).find('[name="action"]').val())

    for (var i = 0, numFiles = reviewsGallery.length; i < numFiles; i++) {
      var file = reviewsGallery[i];
      data.append('foto' + i, file)
    }

    $.ajax({
      type: "POST",
      url: window.ajaxUrl,
      data: data ,
      contentType: false,
      processData: false,
      success: function(msg){
        openModal('#reviewThanks')
        form.reset()
        reviewForm.resetForm()
        $(form).find('.file-group').removeClass('fileUpload').find('.btn').text('Выбрать файл')
      }
    });
    return false;

  }
 });

 $('.send-reviews input[type="file"]').on('input', function () {
   reviewsGallery = this.files
   $(this).closest('.file-group').addClass('fileUpload').find('.btn').text('Файлы загруженны')
 })

 $('.checkbox input').on('change', function() {
   if ($(this).closest('form').hasClass('not-active')) {
     $(this).closest('form').removeClass('not-active')
   } else {
     $(this).closest('form').addClass('not-active')
   }
 })


 $('.fittings-item .open').on('click', function(e) {
   $(this).closest('.fittings-item').addClass('active')
   openFittings($(this).attr('href'));
   return false;
 })

 function openFittings (id) {
   $('#furniture').fadeOut(400);
   $.ajax({
     type: "POST",
     url: window.ajaxUrl,
     data: {action: 'get_fitting', id: id},
     success: function(data){
       data = JSON.parse(data);
       openModal('#furniture')
       $('#furniture').find('.name').text(data.post_title)
       $('#furniture').find('.subname').text(data.subname)
       $('#furniture').find('.modal-furniture-content').text(data.discription)
       $('#furniture').find('.modal-furniture-img').attr('style', 'background-image: url('+data.big_image+')')
     }
   });
 }

 $('.furniture-arrows .arrow-next').on('click', function() {

   var element = $('.fittings-item.active').next()[0]
   var currentElem = $('.fittings-item.active')
   $('.fittings-item.active').removeClass('active')
   if (element) {
     $(element).addClass('active')
     return openFittings($(element).find('.open').attr('href'))
   } else {
     element = $(currentElem).closest('.row').find('.fittings-item')[0]
     $(element).addClass('active')
     return openFittings($(element).find('.open').attr('href'))
   }

 })

 $('.furniture-arrows .arrow-prev').on('click', function() {

   var element = $('.fittings-item.active').prev()[0]
   var currentElem = $('.fittings-item.active')
   $('.fittings-item.active').removeClass('active')
   if (element) {
     $(element).addClass('active')
     return openFittings($(element).find('.open').attr('href'))
   } else {
     element = $(currentElem).closest('.row').find('.fittings-item').last()
     $(element).addClass('active')
     return openFittings($(element).find('.open').attr('href'))
   }

 })
})
