jQuery(document).ready(function () {
  function blockContent(){
    var blockHeight = 0
    $('.block__content').each(function(){
      $(this).attr('style', '')
      if(this.clientHeight > blockHeight) {
        blockHeight = this.clientHeight
      }
    })
    if (window.innerWidth > 600) {
        $('.block__content').css('min-height', blockHeight)
    } else {
      $('.block__content').css('min-height', 0)
    }
  }
  setTimeout(blockContent, 200);
  $(window).resize(blockContent)


  $(".fancybox").fancybox();

  $('.scroll').perfectScrollbar();

  $('input[type="tel"]').mask('(000) 000-0000');
})

jQuery(document).ready(function () {

  $('.burger').on('click', function() {
    var nav = $('.header__nav')
    if (nav.hasClass('active')) {
      closeMenu()
    } else {
      openMenu()
    }
  })

  function openMenu () {
    $('.header__nav').addClass('active')
    $('.header__nav').find('.nav__list').slideDown()
  }

  function closeMenu () {
      $('.header__nav').removeClass('active')
      $('.header__nav').find('.nav__list').slideUp()
  }

  // $('.nav__list>li>a').on('click', function () {
  //   var li = $(this).closest('li')
  //   if (li.hasClass('active')) {
  //     $('.nav__list>li.active').removeClass('active').find('.sub-menu').slideUp()
  //     li.removeClass('active').find('.sub-menu').slideUp()
  //   } else {
  //     $('.nav__list>li.active').removeClass('active').find('.sub-menu').slideUp()
  //     li.addClass('active').find('.sub-menu').slideDown()
  //   }
  //   return false
  // })

  $(document).mouseup(function (e){
		var div = $(".header__nav");
		if (!div.is(e.target)
		    && div.has(e.target).length === 0) {
          if ($('.header__nav').hasClass('active')) {
			         closeMenu()
             }
		}
	});
})

jQuery(document).ready(function () {

  $('.dropdow .value').on('click', function() {
    var parent = $(this).closest('.dropdow')
    if ($(parent).hasClass('active')) {
      $(parent).removeClass('active')
    } else {
      $('.dropdow.active').removeClass('active')
      $(parent).addClass('active')
    }
  })

  $('.dropdow .list li').on('click', function() {
    var val = $(this).text()
    var parent = $(this).closest('.dropdow')

    $(parent).find('.value').text(val)
    $(parent).removeClass('active')
  })

  $('.dillers__dropdown .list li').on('click', function() {
    var map = $(this).attr('data-town')

    $('.dillers__map.active').removeClass('active')
    $(map).addClass('active')
  })
})

jQuery(document).ready(function () {

  setTimeout(function(){
    $('.input-group input, .input-group textarea').on('input', function () {
      var val = $(this).val()
      if (val.length > 0) {
        $(this).closest('div').addClass('active')
      }else {
        $(this).closest('div').removeClass('active')
      }
    })
  }, 1000)
})

// var mapIsLoad = false
// var maps = {}
//
// function initMap() {
//   jQuery(document).ready(function(){
//     if($('.where__tabs').length > 0) {
//       $('.where__tabs .map').each(function() {
//         createMap($(this).attr('data-x'), $(this).attr('data-y'), $(this).attr('id'))
//       })
//     }
//   })
// }
//
// function createMap (x, y, elementId) {
//   console.log(x, y)
//   maps[elementId] = new google.maps.Map(document.getElementById(elementId), {
//     center: {lat: Number(x), lng: Number(y)},
//     zoom: 8
//   });
// }

if (window.ymaps) {
  ymaps.ready(init)
}
var yMaps = {}

function init(){

  jQuery(document).ready(function(){
    if($('.ya-map').length > 0) {
      $('.ya-map').each(function() {
        map($(this).attr('data-x'), $(this).attr('data-y'), $(this).attr('id'))
      })
    }
  })

  function map (x, y, id) {
    // Создание карты.
    yMaps[id] = new ymaps.Map(id, {
        // Координаты центра карты.
        // Порядок по умолчнию: «широта, долгота».
        // Чтобы не определять координаты центра карты вручную,
        // воспользуйтесь инструментом Определение координат.
        center: [Number(x), Number(y)],
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: 16
    });
  }
}

jQuery(document).ready(function($) {
  function openModal (modalId) {
    $('.modal.active').fadeOut().removeClass('active')
    $(modalId).fadeIn().addClass('active')
    $('.overflow').fadeIn()
  }

  function closeModal () {
    $('.modal.active').fadeOut().removeClass('active')
    $('.overflow').fadeOut()
    $('.fittings-item').removeClass('active')
  }

  $('.modal-open').on('click', function(){
    openModal($(this).attr('data-modal'))
    return false;
  })

  $('.modal').on('click', function(e){
    if (e.target !== this)
        return;

    closeModal()
  })

  $('.modal-close, .overflow').on('click', function(){
    closeModal()
  })

  window.openModal = openModal;
})

jQuery(document).ready(function () {
  $('.home__slider').slick({
    dots: true
  })

  $('.products-carusel').slick({
    slidesToShow: 3,
    responsive: [
    {
      breakpoint: 1100,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 1
      }
    }
  ]
  })

  $('.other-reviews').slick({
    slidesToShow: 3,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    }
  ]
  })


  $('.simple-slider').slick({
    slidesToShow: 2,
    responsive: [
    {
      breakpoint: 400,
      settings: {
        slidesToShow: 1
      }
    }]
  })

  $('.designers__slider').slick({

  })
})

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var videos = {}

function onYouTubeIframeAPIReady() {
  jQuery('.video-block').each(function() {
    var videoId = $(this).attr('data-id')
    var video = new YT.Player($(this).find('.video-container').attr('id'), {
      height: '400',
      width: '640',
      videoId: videoId,
      events: {
        'onReady': onPlayerReady
      }
    });

    videos[videoId] = video;
  })
}


// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  jQuery('.video-btn').on('click', function() {
    var videoId = $(this).closest('.video-block').attr('data-id')
    $(this).closest('.video-block').find('.video-preview').fadeOut()
    $(this).fadeOut()
    videos[videoId].playVideo()
  })
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.PLAYING && !done) {
    setTimeout(stopVideo, 6000);
    done = true;
  }
}
function stopVideo() {
  player.stopVideo();
}

jQuery(document).ready(function () {

  var tabClass = '.where__tabs .tab';
  var tabLi = '.where__list li';


  function openTab (tab) {
    if($(tab).length < 1) return false

    $(tabClass+'.active').fadeOut(0).removeClass('active')
    $(tab).fadeIn(0).addClass('active')
    closeMap()
  }

  $(tabLi).on('click', function() {
    $(tabLi+'.active').removeClass('active')
    $(this).addClass('active')
    var tab = $(this).attr('data-tab')
    openTab(tab)
  })

  function openMap (parent) {
    if($(parent).hasClass('open-map')) {
      closeMap()
      return false
    }
    $(parent).addClass('open-map').find('.image').fadeOut()
    $(parent).find('.see-in-map').text('Скрыть карту')
  }

  function closeMap () {
    $(tabClass+'.open-map').find('.image').fadeIn()
    $(tabClass+'.open-map').removeClass('open-map').find('.see-in-map').text('Посмотреть на карте')
  }

  $('.see-in-map').on('click', function(){
    openMap($(this).closest('.tab'))
  })

})

new WOW().init();
