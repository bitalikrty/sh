<?php get_header();?>

<?php
  $currentTaxId =  get_queried_object()->term_id;
  $type = get_field('txt_view', 'categories_'.$currentTaxId)[0];

  switch ($type) {
    case 'view_1':
      get_template_part('products/type_1');
    break;

    case 'view_2':
      get_template_part('products/type_2');
    break;

    case 'view_3':
      get_template_part('products/type_3');
    break;

    case 'view_4':
      get_template_part('products/type_4');
    break;
  }
?>

<?php get_footer();?>
