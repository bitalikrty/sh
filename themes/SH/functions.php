
<?php
get_template_part('functions/option-pages');
get_template_part('functions/post-type');
get_template_part('functions/ajax');
get_template_part('functions/bred');

get_template_part('paths/forms');
get_template_part('paths/head');
get_template_part('paths/text-block');
get_template_part('paths/blocks-w-texts');

register_nav_menus( array(
    'primary' => 'Primary'
) );
add_theme_support( 'custom-logo' );

function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; // поддержка SVG
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 150, 150 ); // default Post Thumbnail dimensions
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'news-feed', 365, 200, true );
}

function add_theme_scripts(){
    wp_deregister_script('jquery');
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/vendor.min.js');
    wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');
    wp_enqueue_script( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js' );
    wp_enqueue_script( 'map', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU', array('vendor') );
    wp_enqueue_script( 'scroll', get_template_directory_uri() . '/assets/js/perfect-scrollbar.jquery.js');
    wp_enqueue_script( 'mask', get_template_directory_uri() . '/assets/js/jquery.mask.js');
    wp_enqueue_script( 'wow', get_template_directory_uri() . '/assets/js/wow.min.js');
    wp_enqueue_script( 'validator-plugin', 'https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js');
    wp_enqueue_script( 'comonjs', get_template_directory_uri() . '/assets/js/common.js');
    wp_enqueue_script( 'validatejs', get_template_directory_uri() . '/assets/js/validate.js');
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function add_theme_style(){
  wp_enqueue_style( 'vendor', get_template_directory_uri() . '/assets/css/vendor.min.css');
  wp_enqueue_style( 'wow', get_template_directory_uri() . '/assets/css/animate.min.css');
  wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
  wp_enqueue_style( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css');
  wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css');
  wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/css/common.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

function deregister_cf7_styles() {
  wp_deregister_style('contact-form-7');
}
add_action('wp_print_styles', 'deregister_cf7_styles', 100);

function remove_menus(){
  remove_menu_page( 'index.php' );                  //Консоль
  // remove_menu_page( 'upload.php' );                 //Медиафайлы
  remove_menu_page( 'edit-comments.php' );          //Комментарии
  remove_menu_page( 'tools.php' );                  //Инструменты
}
add_action( 'admin_menu', 'remove_menus' );
