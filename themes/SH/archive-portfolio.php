<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>


<div class="container">
  <?php page_head(array(
    'title'     => get_field('portfolio_title', 'option'),
    'subtitle'  => get_field('portfolio_subtitle', 'option'),
    'img'       => get_field('portfolio_image', 'option'),
    'small'     => false
  ))?>

  <?php text_block(array(
    'title'    => false,
    'border'   => false,
    'subtitle' => false,
    'text'     => 'Наши производственные мощности и экспертность позволяют браться за реализацию сложных и комплексных решений. Мы радуемся каждой нетривиальной задаче и помогаем воплощать в жизнь Ваши смелые идеи'
  ))?>

  <?php
    $cases = get_posts(array(
      'post_type'=>'portfolio',
      'numberposts' => get_field('portfolio_max_item', 'option')
    ));
  ?>
  <div class="entrence-images mt-50 mb-100">
    <div class="title-block">
      <div class="h1">Портфолио</div>
      <p class="mb-50">проекты, которыми мы гордимся</p>
    </div>
    <div class="row">
      <?php foreach( $cases as $post ): setup_postdata($post);?>
      	<div class="col-6">
          <div class="block">
            <div class="block__img" style="background-image: url(<?=get_the_post_thumbnail_url(get_the_ID(), 'full')?>)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title"><b><?=the_title()?></b></div>
              </div>
              <div class="right"> <a href="<?php the_permalink()?>" class="btn">Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
    	<?php endforeach; wp_reset_postdata();?>
    </div>
  </div>
</div>
<?php get_footer();?>
