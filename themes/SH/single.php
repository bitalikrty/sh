<?php get_header();?>
<?php  the_post();?>
<div class="container">


  <?php page_head(array(
    'title'     => get_the_title(),
    'subtitle'  => false,
    'img'       => get_field('img_after_title'),
    'small'     => true
  ))?>

  <?php text_block(array(
    'title'    => false,
    'border'   => false,
    'subtitle' => false,
    'text'     => apply_filters( 'the_content', get_the_content() )
  ))?>

</div>
<?php get_footer();?>
