<?php get_header(); ?>
<div class="home container">
  <h1 class="wow pulse">Стильные двери для интерьера вашего дома</h1>
  <div class="home__slider slider wow fadeIn">
    <?php
    if( have_rows('slider', 'option') ):
    while ( have_rows('slider', 'option') ) : the_row();?>
        <div class="slider__item" style="background-image: url(<?=the_sub_field('item');?>)"></div>

    <?php endwhile; endif;?>
  </div>
  <div class="home__ul">
    <?php
    if( have_rows('after-slider-list', 'option') ):
    while ( have_rows('after-slider-list', 'option') ) : the_row();?>
        <div class="item wow fadeInLeft"><?php the_sub_field('li');?></div>
    <?php endwhile; endif;?>
  </div>
  <hr class="small center"/>
  <div class="text-block wow fadeInLeft"><?=the_field('home_text', 'option')?></div>
  <hr class="small center"/>
  <div class="home__reasons">
    <div class="row">
      <div class="col-5 home-reasons__left wow bounceInLeft">
        <div class="title-80"><?=the_field('home_reason_title', 'option')?></div>
        <div class="sub-24"><?=the_field('home_reason_subtitle', 'option')?></div>
        <img src="<?=get_field('home_reason_img', 'option')['url']?>"/>
      </div>
      <div class="col-7 home-reasons__right  wow bounceInRight">
        <div class="list-icons">
          <?php
          if( have_rows('reasons', 'option') ):
          while ( have_rows('reasons', 'option') ) : the_row();
          $image = get_sub_field('icon');?>
            <div class="item">
              <div class="icons"> <img src="<?=$image['url']?>"/></div>
              <div class="content">
                <div class="title"><?php the_sub_field('title');?></div>
                <p><?php the_sub_field('text');?></p>
              </div>
            </div>
          <?php endwhile; endif;?>
        </div>
      </div>
    </div>
  </div>
  <hr class="light"/>
  <div class="home__novelties mt-100">
    <div class="title-36">Новинки</div>
    <div class="subtitle-18">созданные на основе актуальных интерьерных тенденций</div>
    <div class="novelities">
      <div class="row">
        <div class="col-6 block__wr wow bounceInUp" data-wow-offset="200">
          <div class="block">
            <div class="block__img" style="background-image: url(<?php bloginfo("template_url"); ?>/assets/img/news/img1.png)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title">Борнео</div>
                <p>Межкомнатные двери современного стиля</p>
              </div>
              <div class="right"> <a class="btn">
                   Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
        <div class="col-6 block__wr wow bounceInUp" data-wow-offset="200" data-wow-delay="0.2s">
          <div class="block">
            <div class="block__img" style="background-image: url(<?php bloginfo("template_url"); ?>/assets/img/news/img2.png)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title">Раффине</div>
                <p>Премиальная коллекция классических дверей</p>
              </div>
              <div class="right"> <a class="btn">
                   Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
        <div class="col-6 block__wr wow bounceInUp" data-wow-offset="200">
          <div class="block">
            <div class="block__img" style="background-image: url(<?php bloginfo("template_url"); ?>/assets/img/news/img3.png)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title">Лакони</div>
                <p>Стеновые панели</p>
              </div>
              <div class="right"> <a class="btn">
                   Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
        <div class="col-6 block__wr wow bounceInUp" data-wow-offset="200" data-wow-delay="0.2s">
          <div class="block">
            <div class="block__img" style="background-image: url(<?php bloginfo("template_url"); ?>/assets/img/news/img4.png)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title">Буазери</div>
                <p>Стеновые панели</p>
              </div>
              <div class="right"> <a class="btn">
                   Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
        <div class="col-6 block__wr wow bounceInUp" data-wow-offset="200">
          <div class="block">
            <div class="block__img" style="background-image: url(<?php bloginfo("template_url"); ?>/assets/img/news/img5.png)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title">Деколайн</div>
                <p>Межкомнатные двери в эмали</p>
              </div>
              <div class="right"> <a class="btn">
                   Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
        <div class="col-6 block__wr wow bounceInUp" data-wow-offset="200" data-wow-delay="0.2s">
          <div class="block">
            <div class="block__img" style="background-image: url(<?php bloginfo("template_url"); ?>/assets/img/news/img6.png)"></div>
            <div class="block__content">
              <div class="left">
                <div class="title">Витражи</div>
                <p>Для дверей и перегородок, потолочные витражи</p>
              </div>
              <div class="right"> <a class="btn">
                   Подробнее<i class="icon icon-right"></i></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="home__bform wow fadeIn">
    <?php Forms::getFormById('Ind-project');?>
  </div>
</div>

<?php get_footer(); ?>
