<?php

function blocks_w_text ($args) {
  $class = '';
  if (isset($args['ad_class'])) {
    $class = $args['ad_class'];
  }
  $html = '<div class="rolling-blocks '.$class.'">';

  foreach ($args['blocks'] as $key => $value) {
    $block = '<div class="rolling">';
    $block .= '<div class="rolling-left"> ';
    $block .= '<div class="title">'.$value['title'].'</div>';
    $block .= '<p>'.$value['text'].'</p>';

    if ($value['file']) {
      $block .= '<a href="'.$value['file'].'" download class="btn">Скачать</a>';
    }
    $block .= '</div>';
    $block .= '<div class="rolling-right rolling-img" style="background-image: url('.$value['img'].')"></div>';
    $block .= '</div>';
    $html .= $block;
  }

  $html .="</div>";

  echo $html;
}
