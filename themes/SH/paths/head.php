<?php

function page_head ($args) {
  $html = '<div class="title-block">';
  $html .= '<div class="h1">'.$args['title'].'</div>';

  if ($args['subtitle'])
    $html .= '<p>'.$args['subtitle'].'</p>';

  $html .= '</div>';
  $html .= '<div class="catalog-head page-head">';

  if ($args['small'] == true)
    $html .= '<div class="catalog-head__img" style="background-image: url('.$args['img'].')"></div>';
  else
    $html .= '<div class="page-head__img" style="background-image: url('.$args['img'].')"></div>';

  $html .= '</div>';
  echo $html;
}
