<?php

/*
*
* $args = {
** container_class
** foto_url
** person_name
** person_work
** title
** subtitle
** form
* }
*/
class Forms {
  __constructor($args) {
    $html = '<div class="bform '.$args['container_class'].'">';
      $html .= '<div class="bform__left">';
        $html .= '<div class="bform__img" style="background-image: url('.$args['foto_url'].')"></div>';
        $html .= '<div class="bform__img-text">';
          $html .= '<div class="title">'.$args['person_name'].'</div><p>'.$args['person_work'].'</p>';
        $html .= '</div>';
      $html .= '<div class="bform__right">';
        $html .= '<div class="title">'.$args['title'].'</div>';
        $html .= '<div class="subtitle">'.$args['subtitle'].'</div>';
        $html .= do_shortcode($args['form']);
      $html .= '</div>';
    $html .= '</div>';
    $this->html = $html;
    return $this;
  }

  public function form () {
    return $this->html;
  }

  private function validArgs ($args) {
    if (!isset($args['foto_url'])) $args['foto_url'] = '/assets/img/persons/alex.png';
    if (!isset($args['person_name'])) $args['person_name'] = 'СИМАКОВ АЛЕКСЕЙ АНАТОЛЬЕВИЧ';
    if (!isset($args['person_work'])) $args['person_work'] = 'руководитель инновационного отдела';
    if (!isset($args['title'])) $args['title'] = 'У Вас есть дизайн-проект?';
    if (!isset($args['subtitle'])) $args['subtitle'] = 'Оставьте заявку и получите подробную информацию от нашего технолога по стоимости и срокам изготовления индивидуальных изделий';
    if (!isset($args['foto_url'])) $args['foto_url'] = '[contact-form-7 id="49" title="Дизайн-проект"]';
  }


}
