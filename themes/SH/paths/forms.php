<?php

/*
*
* $args = {
** container_class
** foto_url
** person_name
** person_work
** title
** subtitle
** form
* }
*/
class Forms {

  function __construct($args = null) {
    $args = self::validArgs($args);
    $html = '<div class="bform '.$args['container_class'].'">';
      $html .= '<div class="bform__left">';
        $html .= '<div class="bform__img" style="background-image: url('.$args['foto_url'].')"></div>';
        $html .= '<div class="bform__img-text">';
          $html .= '<div class="title">'.$args['person_name'].'</div><p>'.$args['person_work'].'</p>';
        $html .= '</div>';
      $html .= '</div>';
      $html .= '<div class="bform__right">';
        $html .= '<div class="title">'.$args['title'].'</div>';
        $html .= '<div class="subtitle">'.$args['subtitle'].'</div>';
        $html .= do_shortcode($args['form']);
      $html .= '</div>';
    $html .= '</div>';
    echo $html;
  }

  private function validArgs ($args) {
    if (!isset($args['container_class'])) $args['container_class'] = '';
    if (!isset($args['foto_url'])) $args['foto_url'] = '/assets/img/persons/alex.png';
    if (!isset($args['person_name'])) $args['person_name'] = 'СИМАКОВ АЛЕКСЕЙ АНАТОЛЬЕВИЧ';
    if (!isset($args['person_work'])) $args['person_work'] = 'руководитель инновационного отдела';
    if (!isset($args['title'])) $args['title'] = 'У Вас есть дизайн-проект?';
    if (!isset($args['subtitle'])) $args['subtitle'] = 'Оставьте заявку и получите подробную информацию от нашего технолога по стоимости и срокам изготовления индивидуальных изделий';
    if (!isset($args['form'])) $args['form'] = '[contact-form-7 id="49" title="Дизайн-проект"]';
    return $args;
  }

  static public function getFormById ($id) {
    $result = array();
    $posts = get_posts(array(
      'post_type' => 'c_forms',
      'numberposts' => 99
    ));
    foreach ($posts as $key => $value) {
      $meta = get_post_meta($value->ID);
      if ($id == $meta['forms_0_id'][0]) {
        $result = array(
          'foto_url' => wp_get_attachment_url($meta['forms_0_foto_url'][0]),
          'person_name' => $meta['forms_0_person_name'][0],
          'person_work' => $meta['forms_0_person_work'][0],
          'title' => $meta['forms_0_title'][0],
          'subtitle' => $meta['forms_0_subtitle'][0],
          'form' => $meta['forms_0_form'][0],
        );
      }
    }

    return new Forms($result);
  }


}
