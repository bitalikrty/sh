<?php

function text_block($args) {
  if (!isset($args['text'])) return false;
  $addClass = $args['border'] ? 'border' : '';
  $html = '<div class="text-block '. $addClass .'">';

  if ($args['border'] != true) {
    $html .= '<hr class="small">';
  }
  if (isset($args['title'])) {
    $html .= '<div class="title-block">';
    $html .= '<div class="h1">'.$args['title'].'</div>';
    $html .= '<p>'.$args['subtitle'].'</p>';
    $html .= '</div>';
  }

  $html .= '<p>'.$args['text'].'</p>';

  if ($args['border'] != true) {
    $html .= '<hr class="small">';
  }

  $html .= '</div>';

  echo $html;
}
