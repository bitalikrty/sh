<div class="modal" id="reviewThanks">
  <div class="modal-content">
    <div class="modal-close"> </div>
    <div class="modal-row row">
      <div class="col-6 foto-block">
        <div class="foto" style="background-image: url(/assets/img/modal/1.png)"></div>
        <div class="name">Позевич Марина Анатольевна</div>
        <p>заместитель генерального директора</p>
      </div>
      <div class="col-6 content-block center">
        <div class="title mb-50">Спасибо за отзыв! </div>
        <p class="mb-50">Если у Вас есть срочный вопрос,<br/>позвоните нам по номеру<a class="big" href="#">8 (423) 224-27-27</a></p>
      </div>
    </div>
  </div>
</div>


<div class="modal" id="backCall">
  <div class="modal-content">
    <div class="modal-close"> </div>
    <div class="modal-row row">
      <div class="col-6 foto-block">
        <div class="foto" style="background-image: url(<?php bloginfo('template_url')?>/assets/img/modal/3.png)"></div>
        <div class="name">Печенкина Юлия Александровна</div>
        <p>консультант фирменного салона</p>
      </div>
      <div class="col-6 content-block center">
        <div class="title mb-25">Обратный звонок<span>Введите свои данные, менеджер свяжется с Вами и ответит на любые интересующие вопросы</span></div>
        <?=do_shortcode('[contact-form-7 id="550" title="Обратный звонок"]')?>
      </div>
    </div>
  </div>
</div>
