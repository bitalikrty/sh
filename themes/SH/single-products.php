<?php get_header();?>

<div class="container breadcrambs">
  <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php
    $type = get_field('product_type');

    switch ($type) {
      case 'type_1':
        get_template_part('products/single_1');
      break;

      case 'type_2':
        get_template_part('products/single_2');
      break;

      case 'type_3':
        get_template_part('products/single_3');
      break;
    }
  ?>
</div>
<?php get_footer();?>
