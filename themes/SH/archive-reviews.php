<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('reviews_title', 'option'),
    'subtitle'  => get_field('reviews_subtitle', 'option'),
    'img'       => get_field('reviews_image', 'option'),
    'small'     => true
  ))?>

  <?php
    $designers_reviews = get_posts(array(
      'post_type' => 'reviews',
      'tax_query' => array(array('taxonomy' => 'reviews_category', 'field' => 'slug','terms' => 'ot-dizajnerov')),
    ));

    $other_reviews = get_posts(array(
      'post_type' => 'reviews',
      'tax_query' => array(array('taxonomy' => 'reviews_category', 'field' => 'slug','terms' => 'ot-ljudej')),
    ));
  ?>

  <div class="reviews mt-50">
    <?php foreach( $designers_reviews as $post ): setup_postdata($post);?>
    <div class="review__wr">
      <div class="review__title"><?=the_title()?></div>
      <div class="review">
        <div class="review__slider home__slider slider slider-light">
          <?php
          $gallery = get_field('review_images');
          foreach ($gallery as $key => $value):?>
            <div class="slider__item review__image" style="background-image: url(<?=$value['url'];?>)"></div>
          <?php endforeach;?>
        </div>
        <div class="review__content">
          <div class="title"><?=the_field('author_name')?><span><?=the_field('text_after_name')?></span></div>
          <?=the_content()?>
        </div>
      </div>
    </div>
    <?php endforeach; wp_reset_postdata();?>
  </div>

  <div class="other-reviews__wr mb-100">
    <div class="other-reviews slick-arrow-side">
      <?php foreach( $other_reviews as $post ): setup_postdata($post);?>
      <div class="item">
        <div class="title"><?=the_title()?></div>
        <?=the_content()?>
        <div class="date"><?=get_the_date('F j, Y');?></div>
      </div>
      <?php endforeach; wp_reset_postdata();?>
    </div>
  </div>

  <div class="new-reviews mb-100">
    <div class="wrap">
      <div class="title">Добавить отзыв</div>
      <div class="subtitle">
        Нам важны Ваши отзывы о сервисе и качестве наших дверей. Ведь именно они помогают нам сегодня быть лучше, чем вчера.</div>
      <form class="send-reviews" enctype = 'multipart/form-data'>
        <input type="hidden" name="action" value="review"/>
        <div class="input-group"> <p>Введите имя</p>
          <input type="text" name="name"/>
        </div>
        <div class="input-group"> <p>Введите город</p>
          <input type="text" name="town"/>
        </div>
        <div class="input-group textareare-group"><p>Введите комментарий</p>
          <textarea name="comment"></textarea>
        </div>
        <div class="file-group">
          <p>Фото выполненного заказа:</p>
          <div class="file">
            <input type="file" name="fotos" multiple/>
            <button class="btn">Выбрать файл</button>
          </div>
        </div>
        <button class="btn btn-green">Добавить отзыв</button>
        <div class="checkbox-group">
          <div class="checkbox">
            <input type="checkbox" checked/><span> </span>
          </div>
          <p> Я согласен на обработку моих персональных данных согласно <a class="green" href="">политике конфиденциальности</a></p>
        </div>
      </form>
    </div>
  </div>
</div>
<?php get_footer();?>
