<?php /* Template Name: Page::Where */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <?php
    $towns = get_field('towns');
    $where_list = [];
    foreach ($towns as $key => $value) {
      $where_list[] = $value['town'];
    }
    $i = 0;
  ?>
  <div class="where mt-100 mb-100">
    <ul class="where__list">
      <?php foreach ($where_list as $key => $value):?>
        <li class="<?php if(!$i) echo 'active'?>" data-tab="#tab<?=$key?>" ><?=$value?></li>
      <?php $i++?>
      <?php endforeach; $i = 0; ?>
    </ul>
    <div class="where__tabs">
      <?php foreach ($towns as $key => $value) :?>
      <div class="tab <?php if(!$i) echo 'active'?>" id="tab<?=$key?>">
        <div class="map__wr">
          <div class="map ya-map" id="map<?=$key?>" data-x="<?=$value['map_x']?>" data-y="<?=$value['map_y']?>"></div>
          <div class="image" style="background-image:url(<?=$value['img']?>)"> </div>
        </div>
        <div class="info">
          <div class="title"><?=$value['town']?></div>
          <p> <b>Адрес:</b> <?=$value['adress']?>
          </p>
          <p><?=$value['work_time']?></p>
          <p><?=$value['contacts']?></p>
          <button class="btn see-in-map">Посмотреть на карте</button>
        </div>
      </div>
      <?php $i++?>
      <?php endforeach ?>
    </div>
  </div>


</div>
<?php get_footer();?>
