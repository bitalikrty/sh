<?php /* Template Name: Page::Technologies */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="principles mb-50">
    <div class="rolling-blocks rolling-blocks__border rolling-blocks__borderImp">
      <div class="rolling lp">
        <div class="rolling-right rolling-img contain" style="background-image: url(<?=the_field('techno_image')?>)"></div>
        <div class="rolling-left">
          <div class="title"><?=the_field('techno_title')?></div>
          <ul class="sq-list">
            <?php
            if( have_rows('techno_list') ):
            while ( have_rows('techno_list') ) : the_row();?>
              <li><?=the_sub_field('li')?></li>
            <?php endwhile; endif;?>
          </ul>
        </div>
      </div>
    </div>

    <?php blocks_w_text(array(
      'blocks' => get_field('blocks_w_texts'),
      'ad_class' => 'rolling-blocks__border lp',
    ))?>
  </div>

</div>
<?php get_footer();?>
