<?php /* Template Name: Page::Texture */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="textures">
    <div class="row">
      <?php
      if( have_rows('block_for_download') ):
      while ( have_rows('block_for_download') ) : the_row();?>
        <div class="col-4 texture">
          <div class="texture__img" style="background: url(<?=the_sub_field('img')?>) no-repeat center"></div>
          <div class="texture__content">
            <p><?=the_sub_field('text')?></p>
            <a href="<?=the_sub_field('file')?>" class="btn" download>Скачать</a>
          </div>
        </div>
      <?php endwhile; endif;?>

    </div>
  </div>

  <div class="home__bform">
    <?php
      $form_id = get_field('form_id');
      $form_name = get_post_meta($form_id)['forms_0_id'][0];
      Forms::getFormById($form_name);
    ?>
  </div>

</div>
<?php get_footer();?>
