<?php /* Template Name: Page::Contacts */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="contacts mb-100">
    <div class="row">
      <?php
        if( have_rows('contacts_r') ):
        while ( have_rows('contacts_r') ) : the_row();
      ?>
      <div class="col-6 contact__item">
        <div class="title"><?=the_sub_field('work')?></div>
        <div class="contact__body">
          <div class="contact__image" style="background-image: url(<?=the_sub_field('img')?>)"></div>
          <div class="contact__content">
            <?php if(get_sub_field('name')):?>
              <div class="name"><?=the_sub_field('name')?></div>
            <?php endif;?>

            <?php if(get_sub_field('email')):?>
              <p> <b>E-mail:</b><?=the_sub_field('email')?></p>
            <?php endif;?>

            <?php if(get_sub_field('tel')):?>
              <p> <b>Телефон:</b>
                <?php while ( have_rows('tel') ) : the_row();?>
                  <a href="tel:<?=the_sub_field('tel')?>"><?=the_sub_field('tel')?></a>
                <?php endwhile;?>
              </p>
            <?php endif;?>
          </div>
        </div>
      </div>
      <?php endwhile;endif;?>
    </div>
  </div>

</div>
<?php get_footer();?>
