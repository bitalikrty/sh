<?php /* Template Name: Page::Designers */ ?>
<?php get_header();?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="designers__slider slick-arrow-side">
    <?php
    if( have_rows('designers_car') ):
    while ( have_rows('designers_car') ) : the_row();?>

      <div class="designers__item">
        <div class="designers__title"><?=the_sub_field('title')?></div>
        <div class="designers__body">
          <div class="image" style="background-image: url(<?=the_sub_field('img')?>)"></div>
          <div class="content">
            <div class="title"><?=the_sub_field('name')?><span> <?=the_sub_field('text_after_name')?></span></div>
            <?=the_sub_field('text')?>
          </div>
        </div>
      </div>

    <?php endwhile; endif;?>

  </div>

  <div class="home__bform">
    <?php
      $form_id = get_field('form_id');
      $form_name = get_post_meta($form_id)['forms_0_id'][0];
      Forms::getFormById($form_name);
    ?>
  </div>


  <div class="entrence-images mt-50 mb-100">
    <div class="title-block">
      <div class="h1">Материалы</div>
      <p class="mb-50">для скачивания и ознакомления</p>
    </div>
    <div class="row">
      <?php
      if( have_rows('materials') ):
      while ( have_rows('materials') ) : the_row();?>
          <div class="col-6">
            <div class="block">
              <div class="block__img" style="background-image: url(<?=the_sub_field('image');?>)"></div>
              <div class="block__content">
                <div class="left">
                  <div class="title"> <b><?=the_sub_field('text');?></b></div>
                </div>
                <div class="right">
                   <a class="btn" href="<?=the_sub_field('url');?>">
                     Подробнее<i class="icon icon-right"></i>
                   </a>
                </div>
              </div>
            </div>
          </div>

      <?php endwhile; endif;?>

    </div>
  </div>
</div>
<?php get_footer();?>
