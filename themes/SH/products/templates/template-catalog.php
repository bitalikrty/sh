<?php /* Template Name: Page::Catalog */ ?>
<?php get_header();?>

<?php
  $parent_category_id = get_field('category_id');
  $taxonomies_ids = get_term_children($parent_category_id, 'categories');
  $taxonomies = [];
  foreach ($taxonomies_ids as $child) {
  	$taxonomies[] = get_term_by( 'id', $child, 'categories' );
  }
?>


<div class="container">
  <?php page_head(array(
    'title'     => get_field('title'),
    'subtitle'  => get_field('subtitle'),
    'img'       => get_the_post_thumbnail_url(get_the_ID(), 'full'),
    'small'     => get_field('small_img')
  ))?>

  <h1 class="margin">Межкомнатные двери</h1>
  <div class="catalog-head">
    <div class="catalog-head__img" style="background-image: url(/assets/img/catalog/head.png)"></div>
    <div class="catalog-filters"><span>По стилю:</span>
      <ul>
        <li> <a href="/katalog  ">Все</a></li>
        <?php foreach ($taxonomies as $key => $value): ?>
          <li><a href="<?=get_term_link($value)?>"><?=$value->name?></a></li>
        <?php endforeach; ?>

      </ul>
    </div>
  </div>

  <?php text_block(array(
    'title'    => get_field('text_block_title'),
    'border'   => get_field('text_block_border'),
    'subtitle' => get_field('text_block_subtitle'),
    'text'     => get_field('text_block_txt')
  ))?>

  <div class="catalog">
  <?php
    foreach ($taxonomies as $key => $value):
    $posts = query_posts(array(
      'tax_query' => array(
    		array(
    			'taxonomy' => 'categories',
    			'field' => 'id',
    			'terms' => $value->term_id
    		)
    	),
      'post_type' => 'products'
    ));
    if ($posts && count($posts) > 0):
  ?>
    <div class="catalog-row">
      <div class="black-title"><?=$value->name?></div>
      <div class="row">
        <?php foreach( $posts as $post ): setup_postdata($post);?>
          <div class="col-4 block__wr">
            <div class="block">
              <div class="block__img" style="background-image: url(<?=get_the_post_thumbnail_url(get_the_ID(), 'full')?>)"></div>
              <div class="block__content">
                <div class="left">
                  <div class="title"><?=the_title()?></div>
                </div>
                <div class="right">
                  <a class="btn" href="<?=the_permalink()?>">Подробнее<i class="icon icon-right"></i>
                   </a>
                </div>
              </div>
            </div>
          </div>
      	<?php endforeach; wp_reset_postdata();?>

      </div>
    </div>
  <?php endif; endforeach; ?>
  </div>


</div>
<?php get_footer();?>
