<?php
  $currentTaxId =  get_queried_object()->term_id;
  $currentTax = get_term_by( 'id', $currentTaxId, 'categories' );
?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">

  <?php page_head(array(
    'title'     => $currentTax->name,
    'subtitle'  => false,
    'img'       => get_field('tax_img', 'categories_'.$currentTaxId),
    'small'     => true
  ))?>

  <?php text_block(array(
    'title'    => false,
    'border'   => false,
    'subtitle' => false,
    'text'     => get_field('tax_text', 'categories_'.$currentTaxId)
  ))?>

  <div class="entrence-images mt-50 mb-100 n-solutions-items">
    <div class="row">
      <?php
        $posts = query_posts(array(
          'tax_query' => array(array('taxonomy' => 'categories', 'field' => 'id', 'terms' => $currentTaxId)),
          'post_type' => 'products'
        ));
      ?>
      <?php foreach( $posts as $post ): setup_postdata($post);?>

      <div class="col-6">
        <div class="block">
          <div class="block__img" style="background-image: url(<?=get_the_post_thumbnail_url(get_the_ID(), 'full')?>)"></div>
          <div class="block__content">
            <div class="left">
              <div class="title"> <b><?=the_title()?></b></div>
              <p><?=the_field('short_discription')?></p>
            </div>
            <div class="right">
              <a class="btn" href="<?=the_permalink()?>">Подробнее<i class="icon icon-right"></i></a>
            </div>
          </div>
        </div>
      </div>

      <?php endforeach; wp_reset_postdata();?>
    </div>
  </div>

</div>
