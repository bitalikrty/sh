<?php page_head(array(
  'title'     => get_the_title(),
  'subtitle'  => get_field('subtitle'),
  'img'       => get_field('head_img'),
  'small'     => false
))?>

<?php text_block(array(
  'title'    => false,
  'border'   => false,
  'subtitle' => false,
  'text'     => get_field('discription')
))?>


<div class="features">
  <div class="title-block">
    <div class="h1">Особенности</div>
    <p class="mb-50"><?=the_field('text_after_title_pluses')?></p>
  </div>
  <ul class="numbers-list">
    <?php
    $i = 0;
    if( have_rows('pluses') ):
    while ( have_rows('pluses') ) : the_row(); $i++?>

      <li> <span class="num"><?=$i?></span>
        <p><?=the_sub_field('li')?></p>
      </li>

    <?php endwhile; endif;?>
  </ul>
  <div class="dark-vn"> При планомерном детальном проектировании Ваша квартира станет поводом для радости и гордости!</div>
</div>
<div class="row transom-image mb-100">
  <?php
  if( have_rows('variants_rs') ):
  while ( have_rows('variants_rs') ) : the_row(); $i++?>

    <div class="col-3">
      <div class="img" style="background-image: url(<?=the_sub_field('img')?>)"></div>
      <p><?=the_sub_field('text')?></p>
    </div>

  <?php endwhile; endif;?>

</div>

<div class="home__bform">
  <?php
    Forms::getFormById('n-solutions');
  ?>
</div>
