<?php
  $currentTaxId =  get_queried_object()->term_id;
  $currentTax = get_term_by( 'id', $currentTaxId, 'categories' );
  $childrensId = get_term_children($currentTaxId, 'categories');

  if (count($childrensId) < 1) {
    $childrensId = array($currentTaxId);
  }

  $taxonomies = [];
  foreach ($childrensId as $child) {
 	  $taxonomies[] = get_term_by( 'id', $child, 'categories' );
  }

  $parent_tax = get_term_by('slug', 'mezhkomnatnye-dveri',  'categories');
  $childrensId = get_term_children($parent_tax->term_id, 'categories');
  $tax_list = array();

  foreach ($childrensId as $child) {
 	  $tax_list[] = get_term_by( 'id', $child, 'categories' );
  }


?>
<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">
  <?php if (get_field('block_w_filter', 'categories_'.$currentTaxId)): ?>

    <h1 class="margin"><?=$currentTax->name?></h1>
    <div class="catalog-head">
      <div class="catalog-head__img" style="background-image: url(<?=get_field('tax_img', 'categories_'.$parent_tax->term_id)?>)"></div>
      <div class="catalog-filters"><span>По стилю:</span>
        <ul>
          <li class="<?php if ($currentTaxId == $parent_tax->term_id) echo 'active'?>">
            <a href="<?=get_term_link($parent_tax)?>">Все</a>
          </li>
          <?php foreach ($tax_list as $key => $value): ?>
            <li class="<?php if ($currentTaxId == $value->term_id) echo 'active'?>">
              <a href="<?=get_term_link($value)?>">
              <?=$value->name?>
              </a>
            </li>
          <?php endforeach; ?>

        </ul>
      </div>
    </div>

  <?php else: ?>
    <?php page_head(array(
      'title'     => $currentTax->name,
      'subtitle'  => false,
      'img'       => get_field('tax_img', 'categories_'.$currentTaxId),
      'small'     => true
    ))?>
  <?php endif;?>

  <?php text_block(array(
    'title'    => false,
    'border'   => false,
    'subtitle' => false,
    'text'     => get_field('tax_text', 'categories_'.$currentTaxId)
  ))?>

  <div class="catalog catalog-3">
  <?php
    foreach ($taxonomies as $key => $value):
    $posts = query_posts(array(
      'tax_query' => array(array('taxonomy' => 'categories', 'field' => 'id','terms' => $value->term_id)),
      'post_type' => 'products',
      'order' => 'ASC'
    ));
    if ($posts && count($posts) > 0):
  ?>
    <div class="catalog-row">
      <div class="black-title"><?=$value->name?></div>
      <div class="row">
        <?php foreach( $posts as $post ): setup_postdata($post);?>
          <div class="col-3 block__wr">
            <div class="block" href="<?=the_field('head_img');?>" data-fancybox="data-fancybox">
              <div class="block__img" style="background-image: url(<?=the_field('head_img');?>)"></div>
              <div class="block__content">
                <div class="left">
                  <div class="title"><?=the_title()?></div>
                </div>
              </div>
            </div>
          </div>
      	<?php endforeach; wp_reset_postdata();?>

      </div>
    </div>
  <?php endif; endforeach; ?>
  </div>

  <div class="home__bform">
    <?php
      Forms::getFormById('Ind-project-2');
    ?>
  </div>

</div>
