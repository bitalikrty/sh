<?php
  $currentTaxId =  get_queried_object()->term_id;
  $currentTax = get_term_by( 'id', $currentTaxId, 'categories');
  $childrensId = get_term_children($currentTaxId, 'categories');
  $taxonomies = [];
  foreach ($childrensId as $child) {
    $tax = get_term_by( 'id', $child, 'categories' );
    $tax->prior = get_field('prior', 'categories_'.$child);
 	  $taxonomies[] = $tax;
  }
  usort($taxonomies, function($a, $b){
    return ($a->prior < $b->prior);
  });
?>

<div class="container breadcrambs">
    <?php kama_breadcrumbs()?>
</div>

<div class="container">

  <?php page_head(array(
    'title'     => $currentTax->name,
    'subtitle'  => false,
    'img'       => get_field('tax_img', 'categories_'.$currentTaxId),
    'small'     => true
  ))?>

  <div class="fittings-discriptions mt-50">
    <div class="row">
      <div class="col-6">
        <p>Мы используем фурнитуру ведущих итальянских производителей, чтобы каждая деталь подчеркивала изысканность и благородство Вашего интерьера.</p>
        <p class="mt-50"> <b>Узнайте подробнее за одну минуту</b></p>
      </div>
      <div class="col-6">
        <div class="video-block" data-id="M7lc1UVf-VE">
          <div class="video-preview" style="background-image:url(<?=get_bloginfo('template_url')?>/assets/img/fittings/video.png)"></div>
          <button class="video-btn"></button>
          <div class="video-container" id="fittings-video"> </div>
        </div>
      </div>
    </div>
  </div>


  <div class="fittings-items mt-50">
    <?php
      foreach ($taxonomies as $key => $value):
      $posts = query_posts(array(
        'tax_query' => array(array('taxonomy' => 'categories', 'field' => 'id','terms' => $value->term_id)),
        'post_type' => 'products'
      ));
      if ($posts && count($posts) > 0):
    ?>
    <div class="black-title"><?=$value->name?></div>
    <div class="row">
      <?php foreach( $posts as $post ): setup_postdata($post);?>
        <div class="col-2 fittings-item pens">
          <a class="body open" data-modal="#furniture" href="<?=the_ID()?>">
            <div class="img">
              <img src="<?=the_field('head_img')?>" alt="">
            </div>
            <p><b><?=the_title()?></b></p>
            <p><?=the_field('subtitle')?></p>
          </a>
        </div>
      <?php endforeach; wp_reset_postdata();?>
    </div>
    <?php endif; endforeach; ?>
  </div>

  <div class="home__bform">
    <?php
      Forms::getFormById('questions_on_fittings');
    ?>
  </div>


  <div class="modal" id="furniture">
    <div class="modal-content">
      <div class="furniture-arrows">
        <div class="arrow-prev"></div>
        <div class="arrow-next"></div>
      </div>
      <div class="modal-close"> </div>
      <div class="modal-furniture-title">
        <span class="name"></span>
        <span class="subname"></span>
      </div>
      <p class="modal-furniture-content"></p>
      <div class="modal-furniture-img" alt=""></div>
    </div>
  </div>
